const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", (event) => {
  const firstName = event.target.value;
  const lastName = txtLastName.value;
  const fullName = `${firstName} ${lastName}`;
  spanFullName.innerHTML = fullName;
});

txtLastName.addEventListener("keyup", (event) => {
  const firstName = txtFirstName.value;
  const lastName = event.target.value;
  const fullName = `${firstName} ${lastName}`;
  spanFullName.innerHTML = fullName;
});



